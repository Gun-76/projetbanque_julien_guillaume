# Projet Banque  

- binome :  
	- Dorne Julien  
	- Grauwin Guillaume  

## Progression  
Projet Non Termin�.  

### Probleme   
Interface Administrateur : option 9 non implementer; option 13,7 non fonctionnelle.  
Interface Conseill� : option 7,10 non implementer; option 11,5 non fonctionnelle.  

## Information  
Projet de Banque.  

Les repertoires sont cr�� automatiquement.  
Pour la m�thodes de domiciliation, le dossier du client est transferer dans la nouvelle agence.  
Lorsqu'un nouveau client est cr�er, un nouveau dossier sera cr�er dans la base de donn�es de l'agence.  
Quand une transaction est effectu�, elle sera g�n�r� dans le ficher transactions du client.  
Les conseill�s sont ajout� dans le fichier listeConseille.txt dans chaque agence.  
Les administrateurs sont ajout� dans le fichier admin a la racine des ressources.  
La liste des clients ne contient que les logins et mots de passes des clients.  

Les Code Source sont dans le fichier jar : Banque_Source.jar.
Le programme est le fichier : Banque.jar.
Les ressources pour les tests sont fournis avec le fichier source .jar

Les mots de passes doivent �tre compos�s que de chiffre et de lettre et sont les m�mes que les logins.  

## Lancement du Programme
Dans le repertoire du jar, lancer la commande : 

	java -jar Banque.jar

Les logs de l'administrateur sont :  
	- login : ADM01  
	- mpd : ADM01  

## T�che  
- [x] Jar Runnable  
- [x] Jar Source  
- [x] Tests

## Fonctionnalit�es  
- [x] Cr�er un Client  
- [x] Cr�er un Conseill�
- [x] Cr�er un Administrateur  
- [x] Cr�er une Agence  
- [x] Cr�er un Compte Courant  
- [x] Cr�er un Livret A  
- [x] Cr�er un PEL  
- [x] Changer un client d'agence  
- [x] G�rer les transactions  
- [x] Affichage d'un menu du Client  
- [x] Affichage d'un menu du Conseill�  
- [x] Affichage d'un menu de l'Administrateur  
- [x] Encodage des mot de passe  
- [x] d�codage des mot de passe  
- [x] cr�ation des dossiers ressources  
- [x] Afficher les infos du client  
- [x] Afficher les comptes du client  
- [ ] Consulter les operations de chaque compte  
- [x] Imprimer les infos du client  

## Test
- [x] TestAgence  
- [x] TestConseille  
- [x] TestCompte  
- [ ] TestAdministrateur  
- [ ] TestClient  
- [x] TestGeneral  
- [x] TestCr�ationDossier  