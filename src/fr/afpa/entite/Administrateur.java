package fr.afpa.entite;

import java.time.LocalDate;
import java.util.ArrayList;

public class Administrateur extends Conseille {
	
	// Atributs de plus que la classe mere Conseille
	private ArrayList<Agence>agence;

	// Constructeur qui reprend les attribts des classes mere Client et Conseille  
	public Administrateur(String nom, String prenom, LocalDate dateDeNaissance, String eMail,String login) {
		super(nom, prenom, dateDeNaissance, eMail, login);
		this.agence = new ArrayList<Agence>();
	}

	// Getter
	public ArrayList<Agence> getAgence() {
		return agence;
	}

	// Setter
	public void setAgence(ArrayList<Agence> agence) {
		this.agence = agence;
	}
	/**
	 * Fonction qui permet de modifier une agence
	 * @param index
	 * @param agence
	 */
	public void setOneAgence(int index,Agence agence) {
		this.agence.set(index, agence);
	}
	/**
	 * Fonction qui retourne une agence
	 * @param index
	 * @return
	 */
	public Agence getAgenceClient(int index) {
		return agence.get(index);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (getClass() != obj.getClass())
			return false;
		Administrateur other = (Administrateur) obj;
		if (agence == null) {
			if (other.agence != null)
				return false;
		} else if (!agence.equals(other.agence))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Administrateur [agence=" + agence + "]";
	}
}
