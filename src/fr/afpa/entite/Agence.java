package fr.afpa.entite;

import java.util.ArrayList;

public class Agence {
	// attributs
	private String codeAgence;
	private String nomAgence;
	private String adressAgence;
	private ArrayList<Conseille>conseille;
	private static int codeAgenceIncrement;
	
	// Constructeur
	public Agence(String nomAgence, String adressAgence) {
		String code = ""+(++codeAgenceIncrement);
		while (code.length() < 3)
			code = '0'+code;
		this.codeAgence = code;
		this.nomAgence = nomAgence;
		this.adressAgence = adressAgence;
		this.conseille = new ArrayList<Conseille>() ;
	}
	
	// Getters
	public String getCodeAgence() {
		return codeAgence;
	}

	public void setCodeAgence(String codeAgence) {
		this.codeAgence = codeAgence;
	}

	public String getNomAgence() {
		return nomAgence;
	}
	
	// Setters
	public void setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
	}

	public String getAdressAgence() {
		return adressAgence;
	}

	public void setAdressAgence(String adressAgence) {
		this.adressAgence = adressAgence;
	}

	public ArrayList<Conseille> getConseille() {
		return conseille;
	}

	public void setConseille(ArrayList<Conseille> conseille) {
		this.conseille = conseille;
	}
	
	// To string
	@Override
	public String toString() {
		return "Agence [codeAgence=" + codeAgence + ", nomAgence=" + nomAgence + ", adressAgence=" + adressAgence
				+ ", conseille=" + conseille + "]";
	}
	/**
	 * Fonction qui permet de modifier un conseille
	 * @param index
	 * @param conseille
	 */
	public void setOneConseille(int index,Conseille conseille) {
		this.conseille.set(index, conseille);
	}
	
	/**
	 * Fonction qui retourne un conseiller
	 * @param index
	 * @return
	 */
	public Conseille getOneConseille(int index) {
		return conseille.get(index);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Agence))
			return false;
		Agence other = (Agence) obj;
		if (adressAgence == null) {
			if (other.adressAgence != null)
				return false;
		} else if (!adressAgence.equals(other.adressAgence))
			return false;
		if (codeAgence == null) {
			if (other.codeAgence != null)
				return false;
		} else if (!codeAgence.equals(other.codeAgence))
			return false;
		if (conseille == null) {
			if (other.conseille != null)
				return false;
		} else if (!conseille.equals(other.conseille))
			return false;
		if (nomAgence == null) {
			if (other.nomAgence != null)
				return false;
		} else if (!nomAgence.equals(other.nomAgence))
			return false;
		return true;
	}
	
}
