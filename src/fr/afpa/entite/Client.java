package fr.afpa.entite;

import java.time.LocalDate;
import java.util.ArrayList;

import fr.afpa.services.ServicesClient;

public class Client extends Personne {

	// Attributs
	private String numClient;
	private String login;
	private static int loginIncrement = 1;
	private boolean active;
	private ArrayList<Compte> compte;
	
	// Constructeur
	public Client(String nom, String prenom, LocalDate dateDeNaissance, String eMail) {
		super(nom, prenom, dateDeNaissance, eMail);
		numClient = ServicesClient.numClient(nom.charAt(0), prenom.charAt(0), loginIncrement);
		this.active = true;
		this.login = String.valueOf(loginIncrement++);	//valueof transforme une valeur en chaine de caratere
			while(this.login.length()<=10) {
				this.login="0"+this.login;
			}
		this.compte = new ArrayList<Compte>();		
	}

	// Getters
	public String getNumClient() {
		return numClient;
	}

	public String getLogin() {
		return login;
	}

	public int getLoginIncrement() {
		return loginIncrement;
	}

	public boolean isActive() {
		return active;
	}

	public ArrayList<Compte> getCompte() {
		return compte;
	}
	// Setters
	public void setNumClient(String numClient) {
		this.numClient = numClient;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public void setCompte(ArrayList<Compte> compte) {
		this.compte = compte;
	}

	
	@Override
	public String toString() {
		return "Client [numClient=" + numClient + ", login=" + login + ", active=" + active + ", compte=" + compte
				+ "]";
	}

	/**
	 * Fonction qui permet de modifier un compte
	 * @param index :	
	 * @param compte
	 */
	public void setOneCompte(int index,Compte compte) {
		this.compte.set(index, compte);
	}
	
	/**
	 * Fonction qui retourne un compte
	 * @param index
	 * @return
	 */
	public Compte getOneCompte(int index) {
		return compte.get(index);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (active != other.active)
			return false;
		if (compte == null) {
			if (other.compte != null)
				return false;
		} else if (!compte.equals(other.compte))
			return false;
		if (login == null) {
			if (other.login != null)
				return false;
		} else if (!login.equals(other.login))
			return false;
		if (numClient == null) {
			if (other.numClient != null)
				return false;
		} else if (!numClient.equals(other.numClient))
			return false;
		return true;
	}
	
	
}
