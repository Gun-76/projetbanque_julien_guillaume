package fr.afpa.entite;

import java.util.Random;

import fr.afpa.control.ControlsCompte;

public class Compte {
	// Attribut(s)
	protected String numero;
	protected double solde;
	protected boolean decouvert;
	protected final double frais = 25;
	protected boolean active;

	// Constructeur(s)
	/**
	 * Constructeur du compte
	 * 
	 * @param decouvert	savoir si le compte peut etre a découvert ou non
	 */
	public Compte(boolean decouvert) {
		this.decouvert = decouvert;
		this.solde = 0.0;
		this.active = true;
		String num = "";
		do {
			num = "";
			for (int i = 0; i < 11; i++) {
				num += new Random().nextInt(10);
			}
		}while (!ControlsCompte.validerNumeroCompte(num));
		this.numero = num;
	}

	// Getter(s)
	/**
	 * obtient le numero du compte
	 * 
	 * @return	le numero du compte
	 */
	public String getNumero() {
		return numero;
	}

	/**
	 * obtient le solde du compte
	 * 
	 * @return	le solde du compte
	 */
	public double getSolde() {
		return solde;
	}

	/**
	 * obtient le decouvert
	 * 
	 * @return	true si le compte accepte le d�couvert, false si non
	 */
	public boolean isDecouvert() {
		return decouvert;
	}

	/**
	 * obtient les frais du compte
	 * 
	 * @return les frais du compte
	 */
	public double getFrais() {
		return frais;
	}

	/**
	 * obtient son activité
	 * 
	 * @return	true si le compte est actif, false si non
	 */
	public boolean isActive() {
		return active;
	}

	// Setter(s)
	/**
	 * modifie le numero du compte
	 * 
	 * @param numero	le nouveau numero
	 */
	public void setNumero(String numero) {
		this.numero = numero;
	}

	/**
	 * modifie le solde du compte
	 * 
	 * @param solde	le nouveau solde
	 */
	public void setSolde(double solde) {
		this.solde = solde;
	}

	/**
	 * change l'autorisation du découvert
	 * 
	 * @param decouvert	la nouvelle autorisation
	 */
	public void setDecouvert(boolean decouvert) {
		this.decouvert = decouvert;
	}

	/**
	 * change l'activit� du compte
	 * 
	 * @param active	la nouvelle activité
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	// Methode(s)
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Compte))
			return false;
		Compte other = (Compte) obj;
		if (active != other.active)
			return false;
		if (decouvert != other.decouvert)
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (Double.doubleToLongBits(solde) != Double.doubleToLongBits(other.solde))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Compte [numero=" + numero + ", solde=" + solde + ", decouvert=" + decouvert + ", frais=" + frais
				+ ", active=" + active + "]";
	}
}
