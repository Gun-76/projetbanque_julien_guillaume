package fr.afpa.entite;

import java.time.LocalDate;
import java.util.LinkedList;

public class Conseille extends Personne {
	
	//	Attribut
	private LinkedList<Client>client;
	private final String login;
	
		// Constructeur
		public Conseille(String nom, String prenom, LocalDate dateDeNaissance, String eMail,String login) {
			super(nom, prenom, dateDeNaissance, eMail);
			this.client = new LinkedList<Client>();
			this.login = login;
		}
		
		// Getter 
		public LinkedList<Client> getClient() {
			return client;
		}
		
		public String getLogin() {
			return login;
		}
		// Setter
		public void setClient(LinkedList<Client> client) {
			this.client = client;
		}
				
		
		/**
		 * Fonction qui permet de modifier un client
		 * @param index
		 * @param client
		 */
		public void setOneClient(int index,Client client) {
			this.client.set(index, client);
		}
		
		/**
		 * Fonction qui retourne un client
		 * @param index
		 * @return
		 */
		public Client getOneClient(int index) {
			return client.get(index);
		}

		@Override
		public String toString() {
			return "Conseille [client=" + client + ", login=" + login + "]";
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (getClass() != obj.getClass())
				return false;
			Conseille other = (Conseille) obj;
			if (client == null) {
				if (other.client != null)
					return false;
			} else if (!client.equals(other.client))
				return false;
			if (login == null) {
				if (other.login != null)
					return false;
			} else if (!login.equals(other.login))
				return false;
			return true;
		}
}
