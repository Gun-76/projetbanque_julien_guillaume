package fr.afpa.entite;

public class LivretA extends Compte {
	//Constructeur(s)
	/**
	 * Constructeur de la classe LivretA
	 * 
	 * @param decouvert	le découvert autorisée ou non
	 */
	public LivretA(boolean decouvert) {
		super(decouvert);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof LivretA))
			return false;
		LivretA other = (LivretA) obj;
		if (active != other.active)
			return false;
		if (decouvert != other.decouvert)
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		if (Double.doubleToLongBits(solde) != Double.doubleToLongBits(other.solde))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "LivretA [numero=" + numero + ", solde=" + solde + ", decouvert=" + decouvert + ", frais=" + frais
				+ ", active=" + active + "]";
	}

}
