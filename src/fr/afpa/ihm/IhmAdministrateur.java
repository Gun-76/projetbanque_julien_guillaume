package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseille;
import fr.afpa.services.ServiceAdministrateur;
import fr.afpa.services.ServiceAgence;
import fr.afpa.services.ServiceCompte;
import fr.afpa.services.ServiceConseille;
import fr.afpa.services.Services;
import fr.afpa.services.ServicesClient;

public class IhmAdministrateur {

	/**
	 * Fonction qui retourne l'affichage du menu d'un administrateur
	 */
	public static void afficherMenuadministrateur() {
		System.out.println("===========Menu Administrateur===========\n" + " 1 - Cr�er une agence\n"+" 2 - cr�er un conseill�\n"
				+ " 3 - Cr�er un client\n" + " 4 - Cr�er un compte\n" + " 5 - Consulter les informations d'un client\n"
				+ " 6 - Consulter les comptes d'un client\n" + " 7 - Consulter les op�rations d'un client\n"
				+ " 8 - Virement\n" + " 9 - Imprimer un relev� d�un compte\n" + "10 - Alimenter un compte\n"
				+ "11 - Retait\n" + "12 - Modifier les informations d�un client\n"
				+ "13 - Changer la domiciliation d�un client\n" + "14 - Activer un compte\n"
				+ "15 - D�sactiver un compte\n" +"16 - Activer un client\n" +"17 - Desactiver un client\n" +"18 - Quitter\n"
				+ "=========================================");
	}

	public static void menuAdmin(Administrateur administrateur, Scanner in) {
		boolean quitter = false;
		do {
			afficherMenuadministrateur();
			int choix = in.nextInt();
			in.nextLine();
			switch (choix) {
			case 1: { 	// Cree une agence
				administrateur.getAgence().add(new ServiceAgence().creerAgence(in));
				Services.initRessources(administrateur);
				break;
			}
			case 2: {	// Cree un conseille
				System.out.println("Entrer le code agence");
				ServiceAdministrateur.rechercheAgence(administrateur, in.nextLine()).getConseille().add(new ServiceConseille().creerConseille(in));
				Services.initRessources(administrateur);
				break;
			}
			case 3: {	// Cree client
				System.out.println("Entrer le login conseille : ");
				Conseille conseille = ServiceAdministrateur.rechercheConseille(administrateur, in.nextLine());
				conseille.getClient().add(new ServicesClient().creeClient(in));
				Services.initRessources(administrateur);
				break;
			}
			case 4: { 	// Cree un compte
				System.out.println("Entrer le numero du client : ");
				Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
				client.getCompte().add(new ServiceCompte().choixCompte(client, in));				
				Services.initRessources(administrateur);
				break;
			}
			case 5: { 	// Consulter les informations d'un client
				System.out.println();
				System.out.println("Entrer le numero du client : ");
				IhmClient.consulterInfoClient(ServiceAdministrateur.rechercheClient(administrateur, in.nextLine()));
				break;
			}
			case 6: {	// Consulter les comptes d'un client
				System.out.println("Entrer le numero du client : ");
				IhmClient.consulterCompteClient(ServiceAdministrateur.rechercheClient(administrateur, in.nextLine()));
				break;
			}
			case 7: {	// Consulter les operations d'un client
				System.out.println("Entrer le numero du client : ");
				String numClient=in.nextLine();
				Client client =ServiceAdministrateur.rechercheClient(administrateur, numClient);
				new Services().consulterOperation(administrateur, client, in);
				break;
			}
			case 8: {	// Virement entre 2 comptes
				System.out.println("Entrer le numero de compte a debiter :");
				System.out.println("Entrer le numero de compte a crediter");
				ServiceAdministrateur.virement(administrateur, in.nextLine(), in.nextLine(), in);
				break;
			}
			case 9: {	// imprimer un releve de compte
				
				
				break;
			}
			case 10: {	// Alimenter un compte
				System.out.println("Entrer le numero de compte du client : ");
				String numCompte = in.nextLine();
				System.out.println("Quel montant voulez-vous ajouter ?");
				double montant = in.nextDouble();
				in.nextLine();
				ServiceAdministrateur.alimenterUnCompte(administrateur, numCompte, montant);
				break;
			}
			case 11: { // Retrait
				System.out.println("Entrer le numero de compte du client : ");
				String numCompte = in.nextLine();
				System.out.println("Quel montant voulez-vous retirer ?");
				double montant = in.nextDouble();
				in.nextLine();
				ServiceAdministrateur.retirerArgent(administrateur, numCompte, montant);
				break;
			}
			case 12: {	// Modifier les informations 
				System.out.println("Entrer le numero du client : ");
				ServicesClient.modifInfoClient(ServiceAdministrateur.rechercheClient(administrateur, in.nextLine()), in);
				break;
			}
			case 13: {	// Changer la domiciliation d'un client
				System.out.println("Entrer le numero de l'agence de domiciliation : ");
					Agence agence1 = ServiceAdministrateur.rechercheAgence(administrateur, in.nextLine());
				System.out.println("Entrer le numero du conseille : ");
					Conseille conseille1 = ServiceAdministrateur.rechercheConseille(administrateur, in.nextLine());
				System.out.println("Entrer le numero du client : ");
					Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
				System.out.println("Entrer le numero de l'agence de destinaion : ");
					Agence agence2 = ServiceAdministrateur.rechercheAgence(administrateur, in.nextLine());
				System.out.println("Entrer le numero de l'agence de destination : ");
					Conseille conseille2 = ServiceAdministrateur.rechercheConseille(administrateur, in.nextLine());
				new ServiceAgence().changerDomiciliation(agence1, conseille1, client, agence2, conseille2);
				break;
			}
			case 14: {	// Activer un compte
				System.out.println("Entrer le numero du client : ");	
					Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
				System.out.println("Entrer le numero de compte que vous voulez activer : ");
				new ServiceCompte().activerUnCompte(client, in.nextLine());
				break;
			}
			case 15: {	// Desactiver un compte
				System.out.println("Entrer le numero du client : ");	
					Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
				System.out.println("Entrer le numero de compte que vous voulez desactiver : ");
				new ServiceCompte().desactiverUnCompte(client, in.nextLine());
				break;
			}
			case 16: {	// Activer un client
				System.out.println("Entrer le numero du client : ");	
				Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
				ServicesClient.activerClient(client);
				break;
			}
			case 17: {	// Desactiver un client
				System.out.println("Entrer le numero du client : ");	
				Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
				ServicesClient.desactiverClient(client);
				break;
			}
			case 18: {	// Quitter
				quitter = true;
				break;
			}
			default: {
				break;
			}
			}
		} while (!quitter);

	}
}
