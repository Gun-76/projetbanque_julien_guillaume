package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Client;
import fr.afpa.entite.LivretA;
import fr.afpa.entite.PEL;
import fr.afpa.services.ServiceAdministrateur;
import fr.afpa.services.Services;

public class IhmClient {
	
	/**
	 * Fonction qui retourne le menu d'affichage du client
	 */
	public static void afficherMenuClient() {
		System.out.println("===============Menu Client==============\n"
				+ "1 - Consulter mes informations\n"
				+ "2 - Consulter mes comptes\n"
				+ "3 - Consulter mes op�rations\n"
				+ "4 - Virement\n"
				+ "5 - Imprimer un relev� d�un compte\n"
				+ "6 - Alimenter un compte\n"
				+ "7 - Retait\n"
				+ "8 - Quitter\n"
				+ "=========================================");
	}

	public static void menuClient(Administrateur administrateur , Client client , Scanner in) {
		boolean quitter = false;
		do {
			IhmClient.afficherMenuClient();
			String choix = in.nextLine();
			switch (choix) {
			case "1": // Consulter les infos d'un client
				consulterInfoClient(client);
				break;
			case "2": // Consulter les infos d'un client et les information de ses comptes
				consulterInfoClient(client);
				consulterCompteClient(client);
				break;
			case "3": 
				//consulterOperation(client , compte);
				new Services().consulterOperation(administrateur, client, in);
				break;
			case "4": 	// Virement
				IhmClient.consulterCompteClient(client);
						System.out.println("Entrer le numero de votre compte a debiter");			
						String comptDeb = in.nextLine();
						System.out.println("Entrer le numero du compte a crediter");
							 String comptCred= in.nextLine();
				ServiceAdministrateur.virement(administrateur, comptDeb, comptCred, in);
				break;
			case "5":	//imprimerReleveCompte(client , compte); + date
				break;
			case "6":	// Alimenter un compte
				System.out.println("Entrer le numero de compte a crediter");
				String numCompte = in.nextLine();
				System.out.println("Quel montant voulez-vous ajouter ?");
				double montant = in.nextDouble();
				in.nextLine();
				ServiceAdministrateur.alimenterUnCompte(administrateur ,  numCompte, montant);
				break;
			case "7": // Retait
				System.out.println("Entrer le numero de compte a debiter");
				String numCompteDeb = in.nextLine();
				System.out.println("Quel montant voulez-vous retirer ?");
				double montant2 = in.nextDouble();
				in.nextLine();
				ServiceAdministrateur.retirerArgent(administrateur , numCompteDeb, montant2);
				break;
			case "8": quitter = true;
				
			default:
				break;
			}
		} while (!quitter);
	}
	public static void afficherModif() {
		System.out.println("Qelle information voulez vous modifier ?\n"
				+ "1 - Nom\n"
				+ "2 - Prenom\n"
				+ "3 - Date de naissance\n"
				+ "4 - E-mail\n"
				+ "Qelle est votre choix ?");
	}
	
	/**
	 * Fonction qui retourne les information d'in client
	 * 
	 * @param client le client concerne 
	 */
	public static void consulterInfoClient(Client client) {
		System.out.println("Information Client\n" + "Numero client" + client.getNumClient() + "\n" + "Nom :"
				+ client.getNom() + "\n" + "Prenom :" + client.getPrenom() + "\n" + "Date de naissance :"
				+ client.getDateDeNaissance() + "\n" + "Adresse mail :" + client.geteMail());
	}
	
	/**
	 * fonction qui retourne l'affichage des information du compte d'un client
	 * 
	 * @param client le client concerne
	 */
	public static void consulterCompteClient(Client client) {
		System.out.println("____________________________________________\n" + "Liste de comptes\n"
				+ "____________________________________________\n" + "Type de compte      Numero de compte     Solde");
		if (client.getCompte().size() != 0) {
			for (int i = 0; i < client.getCompte().size(); i++) {
				if (client.getOneCompte(i) instanceof LivretA) {
					System.out.print("Livret A");
				} else if (client.getOneCompte(i) instanceof PEL) {
					System.out.print("PEL");
				} else {
					System.out.print("Compte courant");
				}
				System.out.print("      " + client.getOneCompte(i).getNumero() + "          "
						+ client.getOneCompte(i).getSolde() + "          ");
				if (client.getOneCompte(i).getSolde() > 0) {
					System.out.println(":-)");
				} else {
					System.out.println(":-(");
				}

			}
		} else {
			System.out.println("Pas de compte !!!!!!!");
		}
	}
	
}	
