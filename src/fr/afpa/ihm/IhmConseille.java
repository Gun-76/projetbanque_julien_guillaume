package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Conseille;
import fr.afpa.services.ServiceAdministrateur;
import fr.afpa.services.ServiceAgence;
import fr.afpa.services.ServiceCompte;
import fr.afpa.services.ServiceConseille;
import fr.afpa.services.Services;
import fr.afpa.services.ServicesClient;

public class IhmConseille {

	/**
	 * Fonction qui retourne l'affichage du menu d'un conseille
	 */
	public static void afficherMenuConseille() {
		System.out.println("============Menu Conseille===============\n"
				+ " 1 - Cr�er un client\n"
				+ " 2 - Cr�er un compte\n"
				+ " 3 - Consulter les informations d'un client\n"
				+ " 4 - Consulter les comptes d'un client\n"
				+ " 5 - Consulter les op�rations d'un client\n"
				+ " 6 - Virement\n"
				+ " 7 - Imprimer un relev� d�un compte\n"
				+ " 8 - Alimenter un compte\n"
				+ " 9 - Retait\n"
				+ "10 - Modifier les informations d�un client\n"
				+ "11 - Changer la domiciliation d�un client\n"
				+ "12 - Quitter\n"
				+ "==========================================");
	}
	public static void menuConseille(Conseille conseille,Administrateur administrateur , Scanner in) {
		boolean quitter = false;
		do {
			afficherMenuConseille();
			String choix = in.next();
			in.nextLine();
			switch (choix) {
			case "1":{  	// Creer un client
				conseille.getClient().add(new ServicesClient().creeClient(in));
				Services.initRessources(administrateur);
				break;
			}
			case "2":{ 	// Cree un compte
				System.out.println("Entrer le numero du client : ");
					Client client = ServiceConseille.rechercheClient(conseille, in.nextLine());
						client.getCompte().add(new ServiceCompte().choixCompte(client, in));
				break;
			}
			case "3":{ 	// Consulter les information d'un client
				System.out.println("Entrer le numero du client : ");
				IhmClient.consulterInfoClient(ServiceConseille.rechercheClient(conseille , in.nextLine()));
				break;
			}
			case "4":{	// Consulter les comptes d'un client
				System.out.println("Entrer le numero du client : ");
				IhmClient.consulterCompteClient(ServiceConseille.rechercheClient(conseille , in.nextLine()));
				break;
			}
			case "5":{	// Consulter les op�rations d'un client
				System.out.println("Entrer le numero du client : ");
				String numClient=in.nextLine();
				Client client =ServiceConseille.rechercheClient(conseille, numClient);
				new Services().consulterOperation(administrateur, client, in);
				break;
			}
			case "6":{	// Virement entre 2 comptes
				System.out.println("Entrer le numero de compte a debiter :");
				System.out.println("Entrer le numero de compte a crediter");
				ServiceAdministrateur.virement(administrateur, in.nextLine(), in.nextLine(), in);
				break;
			}
			case "7":{	// Imprimer un relev� d�un compte
				break;
			}
			case "8":{ 	// Alimenter un compte
				System.out.println("Entrer le numero de compte du client : ");
				String numCompte = in.nextLine();
				System.out.println("Quel montant voulez-vous ajouter ?");
				double montant = in.nextDouble();
				in.nextLine();
				ServiceAdministrateur.alimenterUnCompte(administrateur, numCompte, montant);
				break;
			}
			case "9":{ 	// Retait
				System.out.println("Entrer le numero de compte du client : ");
				String numCompte = in.nextLine();
				System.out.println("Quel montant voulez-vous retirer ?");
				double montant = in.nextDouble();
				in.nextLine();
				ServiceConseille.retirerArgent( administrateur,conseille , numCompte, montant);
				break;
			}
			case "10":{ 	// Modifier les informations d�un client
				System.out.println("Entrer le numero de compte du client : ");	
				break;
			}
			case "11":{	// Changer la domiciliation d�un client
				System.out.println("Entrer le numero de l'agence de domiciliation : ");
				Agence agence1 = ServiceAdministrateur.rechercheAgence(administrateur, in.nextLine());
			System.out.println("Entrer le numero du conseille : ");
				Conseille conseille1 = ServiceAdministrateur.rechercheConseille(administrateur, in.nextLine());
			System.out.println("Entrer le numero du client : ");
				Client client = ServiceAdministrateur.rechercheClient(administrateur, in.nextLine());
			System.out.println("Entrer le numero de l'agence de destinaion : ");
				Agence agence2 = ServiceAdministrateur.rechercheAgence(administrateur, in.nextLine());
			System.out.println("Entrer le numero de l'agence de destination : ");
				Conseille conseille2 = ServiceAdministrateur.rechercheConseille(administrateur, in.nextLine());
			new ServiceAgence().changerDomiciliation(agence1, conseille1, client, agence2, conseille2);
				break;
				}	
			case "12": 	// Quitter
				quitter = true;
				
			default:
				break;
			}
		} while (!quitter);
		
	}
}
