package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entite.Administrateur;
import fr.afpa.services.ServiceAdministrateur;
import fr.afpa.services.Services;
import fr.afpa.services.ServicesClient;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		Administrateur administrateur= new ServiceAdministrateur().creerAdministrateur(in);
		logage(in, administrateur);
		in.close();
	}
		
		
		public static void logage(Scanner in , Administrateur administrateur ) {
			
			do {
				System.out.println("Entrer votre login :");
				String login = in.nextLine();
				System.out.println("Entrer votre mot de pass :");
				String motpass= in.nextLine();
				String log = login+"~"+Services.encodage(motpass);				
				int choix = Services.login(administrateur, log);
				switch (choix) {
				case 0 :	IhmAdministrateur.menuAdmin(administrateur, in);
					break;
				case 1 : 
							IhmConseille.menuConseille(ServiceAdministrateur.rechercheConseille(administrateur , login), administrateur, in);
					break;
				case 2 :	
							IhmClient.menuClient(administrateur, ServicesClient.rechercheClientViaLogin(administrateur, login), in);
					break;
				default: System.out.println("Login incorrect ou mot de passe incorrect !");
					break;
				}
			} while (true);
		}
}
