package fr.afpa.test;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseille;
import fr.afpa.services.ServiceAdministrateur;
import fr.afpa.services.Services;
import junit.framework.TestCase;

public class TestAdministrateur extends TestCase{

	/**
	 * 
	 */
	public TestAdministrateur() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param name
	 */
	public TestAdministrateur(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	@Test
	public void testCreerAdministrateur() throws IOException {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net", "ADM01");
		assertEquals(admin, new ServiceAdministrateur().creerAdministrateur(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
	}

	@Test
	public void testVirementReussite() throws IOException {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		Services.initRessources(admin);
		assertTrue(ServiceAdministrateur.virement(admin, compte.getNumero(), compte2.getNumero(), new Scanner(new File("ressource\\tests\\testAdministrateur\\TestVirementCorrect.txt"))));
	}
	
	@Test
	public void testAlimenterUnCompteActif() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		client.getCompte().add(compte);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		Services.initRessources(admin);
		assertTrue(ServiceAdministrateur.alimenterUnCompte(admin, compte.getNumero(), 50.0));
	}
	
	@Test
	public void testRetirerArgentDecouvertAutoriser() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		client.getCompte().add(compte);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		Services.initRessources(admin);
		assertTrue(ServiceAdministrateur.retirerArgent(admin, compte.getNumero(), 50.0));
	}
	
	@Test
	public void testRetirerArgentDecouvertNonAutoriserReussi() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(false);
		client.getCompte().add(compte);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		Services.initRessources(admin);
		ServiceAdministrateur.alimenterUnCompte(admin, compte.getNumero(), 50.0);
		assertTrue(ServiceAdministrateur.retirerArgent(admin, compte.getNumero(), 50.0));
	}
	
	@Test
	public void testVirementEchecDecouvertNonAutoriser() throws IOException {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(false);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		Services.initRessources(admin);
		assertFalse(ServiceAdministrateur.virement(admin, compte.getNumero(), compte2.getNumero(), new Scanner(new File("ressource\\tests\\testAdministrateur\\TestVirementCorrect.txt"))));
	}
	
	@Test
	public void testCreerAdministrateurIOException() {
		new ServiceAdministrateur().creerAdministrateur(new Scanner("julien\njulien\n30/11/1995\njuline.drone@lpost.net\n"));
	}
	
	@Test
	public void testRechercheAgence() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		admin.getAgence().add(agence);
		Services.initRessources(admin);
		assertEquals(agence,ServiceAdministrateur.rechercheAgence(admin, "001"));
	}
	
	@Test
	public void testRechercheConseille() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		Services.initRessources(admin);
		assertEquals(conseille,ServiceAdministrateur.rechercheConseille(admin, conseille.getLogin()));
	}
	
	@Test
	public void testRechercheClient() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		Services.initRessources(admin);
		assertEquals(client, ServiceAdministrateur.rechercheClient(admin, client.getNumClient()));
	}
	
	@Test
	public void testRechercheCompte() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(false);
		client.getCompte().add(compte);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		Services.initRessources(admin);
		assertEquals(compte, ServiceAdministrateur.rechercheCompte(admin, compte.getNumero()));
	}
	
	@Test
	public void testAttributsGetterSetter() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		assertEquals("Dorne",admin.getNom());
		assertEquals("Julien",admin.getPrenom());
		assertEquals(LocalDate.now(),admin.getDateDeNaissance());
		assertEquals("julien.dorne@laposte.net",admin.geteMail());
		assertEquals("ADM01",admin.getLogin());
		admin.setNom("1");
		admin.setPrenom("1");
		admin.setDateDeNaissance(LocalDate.now().minusDays(1));
		admin.seteMail("juien.done@laposte.net");
		assertEquals("1",admin.getNom());
		assertEquals("1",admin.getPrenom());
		assertEquals(LocalDate.now().minusDays(1),admin.getDateDeNaissance());
		assertEquals("juien.done@laposte.net",admin.geteMail());
	}
	
	@Test
	public void testEquals() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Administrateur admin1 = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		assertEquals(admin, admin);
		assertEquals(admin, admin1);
	}
}
