package fr.afpa.test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseille;
import fr.afpa.services.ServiceAgence;
import junit.framework.TestCase;

public class TestAgence extends TestCase{

	@Test
	public void testCreerAgence() throws IOException {
		Agence agence = new Agence("Test", "Test");
		assertEquals(agence, new ServiceAgence().creerAgence(new Scanner("Test\nTest\n")));
	}
	
	@Test
	public void testChangerDomiciliation() {
		Agence agence1 = new Agence("Test", "Test");
		Agence agence2 = new Agence("Test", "Test");
		Conseille conseille11 = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net", "CO0001");
		Conseille conseille12 = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net", "CO0001");
		Conseille conseille21 = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net", "CO0001");
		Conseille conseille22 = new Conseille("Dorne", "Jul", LocalDate.of(1995, 11, 30), "jul.Dorne@laposte.net", "CO0002");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Client client1 = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		conseille12.getClient().add(client1);
		conseille12.getClient().add(client);
		agence1.getConseille().add(conseille11);
		agence1.getConseille().add(conseille12);
		agence2.getConseille().add(conseille21);
		agence2.getConseille().add(conseille22);
		assertTrue(new ServiceAgence().changerDomiciliation(agence1, conseille12, client, agence2, conseille22));
	}
	
	@Test
	public void testChangerDomiciliationNullAgence1() {
		assertTrue(new ServiceAgence().changerDomiciliation(null, null, null, null, null));
	}
	
	@Test
	public void testChangerDomiciliationNullConseille2() {
		Agence agence1 = new Agence("Test", "Test");
		Agence agence2 = new Agence("Test", "Test");
		Conseille conseille1 = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		assertTrue(new ServiceAgence().changerDomiciliation(agence1, conseille1, client, agence2, null));
	}
	
	@Test
	public void testChangerDomiciliationNullAgence2() {
		Agence agence1 = new Agence("Test", "Test");
		Conseille conseille1 = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		assertTrue(new ServiceAgence().changerDomiciliation(agence1, conseille1, client, null, null));
	}
	
	@Test
	public void testChangerDomiciliationMemeAgence() {
		Agence agence1 = new Agence("Test", "Test");
		Conseille conseille1 = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		assertTrue(new ServiceAgence().changerDomiciliation(agence1, conseille1, client, agence1, null));
	}
	
	@Test
	public void testChangerDomiciliationNullClient() {
		Agence agence1 = new Agence("Test", "Test");
		Conseille conseille1 = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net", "CO0001");
		assertTrue(new ServiceAgence().changerDomiciliation(agence1, conseille1, null, null, null));
	}
	
	@Test
	public void testChangerDomiciliationNullConseille1() {
		Agence agence1 = new Agence("Test", "Test");
		assertTrue(new ServiceAgence().changerDomiciliation(agence1, null, null, null, null));
	}

	@Test
	public void testVirementReussite() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		assertTrue(ServiceAgence.virement(admin, agence, compte.getNumero(), compte2.getNumero(), new Scanner("50\n")));
	}
	
	@Test
	public void testVirementEchecDecouvertNonAutoriser() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(false);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		assertFalse(ServiceAgence.virement(admin, agence, compte.getNumero(), compte2.getNumero(), new Scanner("50\n")));
	}
	
	@Test
	public void testGetterSetter() {
		Agence agence = new Agence("test", "test");
		agence.setCodeAgence("000");
		assertEquals("000",agence.getCodeAgence());
		assertEquals("test",agence.getAdressAgence());
		assertEquals("test",agence.getNomAgence());
		agence.setAdressAgence("1");
		agence.setNomAgence("0");
		assertEquals("000",agence.getCodeAgence());
		assertEquals("1",agence.getAdressAgence());
		assertEquals("0",agence.getNomAgence());
	}
	
	@Test
	public void testEquals() {
		Agence agence = new Agence("test", "test");
		Agence agence1 = new Agence("test", "test");
		Agence agence2 = new Agence("test", "test");
		agence1.setCodeAgence(agence.getCodeAgence());
		assertEquals(agence, agence1);
		assertEquals(agence, agence);
		assertNotEquals(agence1, agence2);
		assertNotEquals(agence, 32);
		agence.setAdressAgence(null);
		assertNotEquals(agence, agence1);
		agence.setAdressAgence("test");
		agence.setCodeAgence(null);
		assertNotEquals(agence, agence1);
		agence.setCodeAgence(agence1.getCodeAgence());
		agence.setNomAgence(null);
		assertNotEquals(agence, agence1);
		
	}
}
