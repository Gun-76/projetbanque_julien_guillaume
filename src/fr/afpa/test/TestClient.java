package fr.afpa.test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseille;
import fr.afpa.services.ServicesClient;
import junit.framework.TestCase;

public class TestClient extends TestCase{

	@Test
	public void testCreerClient() throws IOException {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net");
		client.setLogin("00000000005");
		client.setNumClient("DJ000005");
		assertEquals(client, new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
	}
	
	@Test
	public void testNbCompteActif( ) {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net");
		Compte compte = new Compte(true);
		client.getCompte().add(compte);
		assertEquals(1, ServicesClient.nbCompteactiv(client.getCompte()));
	}
	
	@Test
	public void testActiverClientReussi( ) {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net");
		client.setActive(false);
		assertTrue(!client.isActive());
		assertTrue(ServicesClient.activerClient(client));
	}
	
	@Test
	public void testActiverClientEchec( ) {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net");
		assertTrue(client.isActive());
		assertFalse(ServicesClient.activerClient(client));
	}
	
	@Test
	public void testDesactiverClientReussi( ) {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net");
		assertTrue(client.isActive());
		assertTrue(ServicesClient.desactiverClient(client));
	}
	
	@Test
	public void testDesactiverClientEchec( ) {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net");
		client.setActive(false);
		assertTrue(!client.isActive());
		assertFalse(ServicesClient.desactiverClient(client));
	}
	
	@Test
	public void testAlimenterUnCompteActif() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		client.getCompte().add(compte);
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		assertTrue(ServicesClient.alimenterUnCompte(admin, client, compte.getNumero(), 50.0));
	}
	
	@Test
	public void testAlimenterUnCompteNonActif() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		compte.setActive(false);
		client.getCompte().add(compte);
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		assertFalse(ServicesClient.alimenterUnCompte(admin, client, compte.getNumero(), 50.0));
	}
	
	@Test
	public void testAlimenterUnCompteActifNegatif() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		compte.setActive(false);
		client.getCompte().add(compte);
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		assertFalse(ServicesClient.alimenterUnCompte(admin, client, compte.getNumero(), -50.0));
	}
	
	@Test
	public void testRetirerArgentDecouvertAutoriser() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		client.getCompte().add(compte);
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		assertTrue(ServicesClient.retirerArgent(admin, client, compte.getNumero(), 50.0));
	}
	
	@Test
	public void testRetirerArgentDecouvertNonAutoriserReussite() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(false);
		client.getCompte().add(compte);
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		ServicesClient.alimenterUnCompte(admin, client, compte.getNumero(), 50.0);
		assertTrue(ServicesClient.retirerArgent(admin, client, compte.getNumero(), 50.0));
	}
	
	@Test
	public void testRetirerArgentDecouvertNonAutoriserEchec() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		compte.setActive(false);
		client.getCompte().add(compte);
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		assertFalse(ServicesClient.retirerArgent(admin, client, compte.getNumero(), 50.0));
	}
	
	@Test
	public void testModifInfoClient() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		ServicesClient.modifInfoClient(client, new Scanner("0\n1\nDorne\n"));
		ServicesClient.modifInfoClient(client, new Scanner("2\nJulien\n"));
		ServicesClient.modifInfoClient(client, new Scanner("3\n30/11/1995\n"));
		ServicesClient.modifInfoClient(client, new Scanner("4\njulien.dorne@laposte.net\n"));
	}
	
	@Test
	public void testRechercheClientViaLogin() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(false);
		client.getCompte().add(compte);
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		assertEquals(client, ServicesClient.rechercheClientViaLogin(admin, client.getLogin()));
	}
}
