package fr.afpa.test;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.PEL;
import fr.afpa.services.ServiceCompte;
import junit.framework.TestCase;

public class TestCompte extends TestCase{

	@Test
	public void testActiverUnCompteReussite() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		client.getCompte().add(compte);
		compte.setActive(false);
		assertFalse(compte.isActive());
		assertTrue(new ServiceCompte().activerUnCompte(client, compte.getNumero()));
	}
	
	@Test
	public void testActiverUnCompteEchec() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		client.getCompte().add(compte);
		assertTrue(compte.isActive());
		assertFalse(new ServiceCompte().activerUnCompte(client, compte.getNumero()));
	}

	@Test
	public void testDesactiverUnComptePELReussite() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new PEL(true);
		client.getCompte().add(compte);
		assertTrue(compte.isActive());
		assertTrue(new ServiceCompte().desactiverUnCompte(client, compte.getNumero()));
	}
	
	@Test
	public void testDesactiverUnCompteEchec() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		compte.setActive(false);
		client.getCompte().add(compte);
		assertFalse(new ServiceCompte().desactiverUnCompte(client, compte.getNumero()));
	}
	
	@Test
	public void testCalculerFraisCompteCourant() {
		Compte compte = new Compte(true);
		assertEquals(25.0, new ServiceCompte().CalculerFrais(compte));
	}
	
	@Test
	public void testCreerCompteCourant() throws IOException {
		Compte compte = new Compte(true);
		Compte compte2 = new ServiceCompte().creerCompte(new Scanner("o\n"));
		compte.setNumero(compte2.getNumero());
		assertEquals(compte, compte2);
	}
	
	@Test
	public void testChoixCompte() {
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Client client2 = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		client2.getCompte().add(new Compte(true));
		new ServiceCompte().choixCompte(client, new Scanner("o\n"));
		new ServiceCompte().choixCompte(client, new Scanner("1\no\n"));
		new ServiceCompte().choixCompte(client, new Scanner("2\no\n"));
		new ServiceCompte().choixCompte(client2, new Scanner("3\no\n"));
		new ServiceCompte().choixCompte(client2, new Scanner("4\n"));
	}
}
