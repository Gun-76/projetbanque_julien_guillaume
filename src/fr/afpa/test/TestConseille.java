package fr.afpa.test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseille;
import fr.afpa.services.ServiceConseille;
import junit.framework.TestCase;

public class TestConseille extends TestCase{

	@Test
	public void testCreerConseilleCorrect() throws IOException {
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net", "CO0001");
		assertEquals(conseille, new ServiceConseille().creerConseille(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
	}
	
	@Test
	public void testCreerConseilleIncorrect() throws IOException {
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net", "CO0001");
		assertNotEquals(conseille, new ServiceConseille().creerConseille(new Scanner("Dorne\nJulien\n30/11/1996\njulien.dorne@laposte.net\n")));
	}
	
	@Test
	public void testRechercheCompte() {
		Conseille conseille = new Conseille("al", "al", LocalDate.now(), "truc.bidule@hotmail.fr", "CO0001");
		Compte compte = new Compte(true);
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		conseille.getClient().add(client);
		client.getCompte().add(compte);
		assertEquals(compte, ServiceConseille.rechercheCompte(conseille, compte.getNumero()));
	}
	
	@Test
	public void testRechercheClient() {
		Conseille conseille = new Conseille("al", "al", LocalDate.now(), "truc.bidule@hotmail.fr", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		conseille.getClient().add(client);
		assertEquals(client, ServiceConseille.rechercheClient(conseille, client.getNumClient()));
	}

}
