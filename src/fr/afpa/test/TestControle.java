package fr.afpa.test;

import java.io.IOException;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import fr.afpa.control.ControlsAdministrateur;
import fr.afpa.control.ControlsAgence;
import fr.afpa.control.ControlsClient;
import fr.afpa.control.ControlsCompte;
import fr.afpa.control.ControlsConseille;
import fr.afpa.control.ControlsGeneral;
import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseille;
import fr.afpa.services.Services;
import junit.framework.TestCase;

public class TestControle extends TestCase {

	Administrateur admin;
	Agence agence;
	Client client;
	Conseille conseille;
	Compte compte;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		admin = new Administrateur("Julien", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "ADM01");
		agence = new Agence("001", "001");
		client = new Client("dorne", "julien", LocalDate.now(), "julien.dorne@laposte.net");
		conseille = new Conseille("dorne", "julien", LocalDate.now(), "julien.dorne@laposte.net", "CO0001");
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		Services.initRessources(admin);
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

	@Test
	public void testAdminValiderLogin() {
		assertTrue(ControlsAdministrateur.validerLogin("ADM01"));
		assertFalse(ControlsAdministrateur.validerLogin("tryc�"));
	}

	@Test
	public void testAgenceValiderAddresseAgence() {
		assertTrue(ControlsAgence.validerAddresseAgence("truc"));
		assertFalse(ControlsAgence.validerAddresseAgence("0"));
	}
	
	@Test
	public void testAgenceValidNomAgence() {
		assertTrue(ControlsAgence.validNomAgence("truc"));
		assertFalse(ControlsAgence.validNomAgence("0"));
	}
	
	@Test
	public void testClientValidNomPrenom() {
		assertTrue(ControlsClient.validNomPrenom("truc"));
		assertFalse(ControlsClient.validNomPrenom("0"));
	}
	
	@Test
	public void testClientValidNumClient() {
		assertTrue(ControlsClient.validnumClient("AB000001"));
		assertFalse(ControlsClient.validnumClient("0"));
	}
	
	@Test
	public void testClientControleNumeroClient() {
		assertTrue(ControlsClient.controleNumeroClient("AB000001"));
		assertFalse(ControlsClient.controleNumeroClient("AB000001"));
	}
	
	@Test
	public void testCompteValiderNumeroCompte() {
		assertTrue(ControlsCompte.validerNumeroCompte("00000000000"));
		assertFalse(ControlsCompte.validerNumeroCompte("0"));
	}
	
	@Test
	public void testConseilleValiderLogin() {
		assertTrue(ControlsConseille.validerLogin("CO01"));
		assertFalse(ControlsConseille.validerLogin("0"));
	}
	
	@Test
	public void testGeneralValidDate() {
		assertTrue(ControlsGeneral.validDate("30/11/1995"));
		assertFalse(ControlsGeneral.validDate("11/1995"));
	}
	
	@Test
	public void testGeneralValidEMail() {
		assertTrue(ControlsGeneral.validEMail("julien.dorne@laposte.net"));
		assertFalse(ControlsGeneral.validEMail("oz"));
	}
	
	@Test
	public void testGeneralVerificationLoginClient() throws IOException {
		assertTrue(ControlsGeneral.verificationLoginClient(agence, client.getLogin(), client.getLogin()));
		assertFalse(ControlsGeneral.verificationLoginClient(agence, "01234", "0123456789"));
	}
	
	@Test
	public void testGeneralVerificationLoginConseille() throws IOException {
		assertTrue(ControlsGeneral.verificationLoginConseille(agence, conseille.getLogin(), conseille.getLogin()));
		assertFalse(ControlsGeneral.verificationLoginConseille(agence, "01234", "0123456789"));
	}
	
	@Test
	public void testGeneralVerificationLoginAdmin() throws IOException {
		assertTrue(ControlsGeneral.verificationLoginAdmin("ADM01", "ADM01"));
		assertFalse(ControlsGeneral.verificationLoginAdmin("01234", "0123456789"));
	}
	
	@Test
	public void testGeneralValidTransacDate() {
		assertTrue(ControlsGeneral.validTransacDate("30/11/1990", "30/11/1992", "30-11-1991"));
		assertFalse(ControlsGeneral.validTransacDate("30/11/1990", "30/11/1990", "30-11-1991"));
	}
	
}
