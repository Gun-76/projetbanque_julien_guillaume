package fr.afpa.test;

import java.io.IOException;
import java.util.Scanner;

import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Personne;
import fr.afpa.services.ServiceAdministrateur;
import fr.afpa.services.ServiceAgence;
import fr.afpa.services.ServiceCompte;
import fr.afpa.services.ServiceConseille;
import fr.afpa.services.ServicePEL;
import fr.afpa.services.Services;
import fr.afpa.services.ServicesClient;
import junit.framework.TestCase;

public class TestCreationRessource extends TestCase{
	public static void main(String[] args) {
		try {
			Personne admin = new ServiceAdministrateur().creerAdministrateur(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n"));
			((Administrateur)admin).getAgence().add(new ServiceAgence().creerAgence(new Scanner("Dorne\nJulien\n")));
			((Administrateur)admin).getAgenceClient(0).getConseille().add(new ServiceConseille().creerConseille(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(0).getConseille().add(new ServiceConseille().creerConseille(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(0).getOneConseille(0).getClient().add(new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(0).getOneConseille(0).getClient().add(new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(0).getOneConseille(0).getClient().add(new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(0).getOneConseille(1).getClient().add(new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(0).getOneConseille(1).getClient().add(new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			
			((Administrateur)admin).getAgenceClient(0).getOneConseille(0).getOneClient(0).getCompte().add(new ServiceCompte().creerCompte(new Scanner("o\n")));

			((Administrateur)admin).getAgence().add(new ServiceAgence().creerAgence(new Scanner("Dorne\nJulien\n")));
			((Administrateur)admin).getAgenceClient(1).getConseille().add(new ServiceConseille().creerConseille(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(1).getConseille().add(new ServiceConseille().creerConseille(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(1).getOneConseille(0).getClient().add(new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(1).getOneConseille(0).getClient().add(new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(1).getOneConseille(0).getClient().add(new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(1).getOneConseille(1).getClient().add(new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			((Administrateur)admin).getAgenceClient(1).getOneConseille(1).getClient().add(new ServicesClient().creeClient(new Scanner("Dorne\nJulien\n30/11/1995\njulien.dorne@laposte.net\n")));
			
			((Administrateur)admin).getAgenceClient(0).getOneConseille(1).getOneClient(0).getCompte().add(new ServiceCompte().creerCompte(new Scanner("o\n")));
			((Administrateur)admin).getAgenceClient(0).getOneConseille(1).getOneClient(0).getCompte().add(new ServicePEL().creerPEL(new Scanner("o\n")));
			Services.initRessources((Administrateur) admin);
			System.out.println("Quel montant voulez-vous ajouter ?");
			double montant = 10;
			ServiceAdministrateur.alimenterUnCompte((Administrateur) admin, ((Administrateur)admin).getAgenceClient(0).getOneConseille(1).getOneClient(0).getCompte().get(0).getNumero(), montant);
			Services.domiciliationClient(((Administrateur)admin).getAgenceClient(0), ((Administrateur)admin).getAgenceClient(1), ((Administrateur)admin).getAgenceClient(0).getOneConseille(1).getOneClient(0));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
