package fr.afpa.test;

import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.Administrateur;
import fr.afpa.entite.Agence;
import fr.afpa.entite.Client;
import fr.afpa.entite.Compte;
import fr.afpa.entite.Conseille;
import fr.afpa.ihm.IhmAdministrateur;
import fr.afpa.ihm.IhmClient;
import fr.afpa.ihm.IhmConseille;
import fr.afpa.ihm.Main;
import fr.afpa.services.Services;
import junit.framework.TestCase;

public class TestGeneral extends TestCase{

	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}
	
	@Test
	public void testEncodage() {
		assertEquals(Services.encodage(new String("abc")), new String("def"));
	}

	@Test
	public void testDecodage() {
		assertEquals(Services.decodage(new String("def")), new String("abc"));
	}
	
	@Test
	public void testCreationRessource() {
		TestCreationRessource.main(null);
	}
	
	
	@Test
	public void testChemin() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		assertEquals("ressource\\001\\clients\\"+client.getNumClient()+"\\transactions.txt", new Services().chemin(agence.getCodeAgence(), client.getNumClient()));
	}
	
	@Test
	public void testTransact() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		assertEquals(new ArrayList<String>(), new Services().transact(agence.getCodeAgence(), client.getNumClient(), new Scanner("01/01/1000\n01/01/1001\n")));
	}
	
	@Test
	public void testConsulterOperation() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		new Services().consulterOperation(admin, client, new Scanner("01/01/1000\n01/01/1001\n"));
	}
	
	@Test
	public void testOperations() {
		assertEquals("Retrait ", new Services().operation("R"));
		assertEquals("Credit  ", new Services().operation("A"));
		assertEquals("Virement", new Services().operation("V"));
	}
	
	@Test
	public void testTypeCompte() {
		assertEquals("Compte courant", new Services().typeCompte("Compte"));
		assertEquals("Livret A      ", new Services().typeCompte("livretA"));
		assertEquals("P.E.L.        ", new Services().typeCompte("PEL"));
	}
	
	@Test
	public void testConsulterCompteClient() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		IhmClient.consulterCompteClient(client);
	}
	
	@Test
	public void testConsulterInfosClient() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		IhmClient.consulterInfoClient(client);
	}
	
	@Test
	public void testMenuClient() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		IhmClient.menuClient(admin, client, new Scanner("1\n2\n3\n30/11/1995\n30/11/1995\n4\n"));
	}
	
	@Test
	public void testMenuClient5() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		IhmClient.menuClient(admin, client, new Scanner("5\n6\n"));
	}
	
	@Test
	public void testMenuClient6() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		IhmClient.menuClient(admin, client, new Scanner("7\n"));
	}
	
	@Test
	public void testMenuClient7() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		IhmClient.menuClient(admin, client, new Scanner("8\n"));
	}
	
	@Test
	public void testMenuConseille() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		Client client = new Client("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.Dorne@laposte.net");
		Compte compte = new Compte(true);
		Compte compte2 = new Compte(true);
		client.getCompte().add(compte);
		client.getCompte().add(compte2);
		conseille.getClient().add(client);
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		IhmConseille.menuConseille(conseille, admin, new Scanner("7\n12\n"));
	}
	
	@Test
	public void testMain0() throws FileNotFoundException {
		Administrateur admin = new Administrateur("Julien", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "ADM01");
		Services.initRessources(admin);
		Main.logage(new Scanner("ADM01\nADM01\n18\n"), admin);
		
	}
	
	@Test
	public void testMain1() throws FileNotFoundException {
		Administrateur admin = new Administrateur("Julien", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "ADM01");
		Agence agence = new Agence("test", "test");
		Conseille conseille = new Conseille("Dorne", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "CO0001");
		agence.getConseille().add(conseille);
		admin.getAgence().add(agence);
		Services.initRessources(admin);
		Main.logage(new Scanner("CO0001\nCO0001\n12\n"), admin);
		
	}
	
	@Test
	public void testMain2() throws FileNotFoundException {
		Administrateur admin = new Administrateur("Julien", "Julien", LocalDate.of(1995, 11, 30), "julien.dorne@laposte.net", "ADM01");
		Services.initRessources(admin);
		Main.logage(new Scanner("00000000001\n00000000001\n8\n"), admin);
		
	}

	@Test
	public void testMenuAdmin() {
		Administrateur admin = new Administrateur("Dorne", "Julien", LocalDate.now(), "julien.dorne@laposte.net", "ADM01");
		Services.initRessources(admin);
		IhmAdministrateur.menuAdmin(admin, new Scanner("3\n001\n"));
	}
}
