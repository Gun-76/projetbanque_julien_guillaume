package fr.afpa.test;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.LivretA;
import fr.afpa.services.ServiceCompte;
import fr.afpa.services.ServiceLivretA;
import junit.framework.TestCase;

public class TestLivretA extends TestCase{

	@Test
	public void testCreerCompteLivretA() throws IOException {
		Scanner sc = new Scanner(new File("ressource\\tests\\testCompte\\TestCreerLivretA.txt"));
		LivretA compte = new LivretA(true);
		LivretA compte2 = new ServiceLivretA().creerLivretA(sc);
		compte.setNumero(compte2.getNumero());
		assertEquals(compte, compte2);
		sc.close();
	}

	@Test
	public void testCalculerFraisCompteLivretA() {
		LivretA compte = new LivretA(true);
		assertEquals(25.0, new ServiceCompte().CalculerFrais(compte));
		compte.setSolde(100.0);
		assertEquals(35.0, new ServiceCompte().CalculerFrais(compte));
	}
}
