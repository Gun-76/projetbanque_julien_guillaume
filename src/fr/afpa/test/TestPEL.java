package fr.afpa.test;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import fr.afpa.entite.PEL;
import fr.afpa.services.ServiceCompte;
import fr.afpa.services.ServicePEL;
import junit.framework.TestCase;

public class TestPEL extends TestCase{

	@Test
	public void testCreerComptePEL() throws IOException {
		Scanner sc = new Scanner(new File("ressource\\tests\\testCompte\\TestCreerPEL.txt"));
		PEL compte = new PEL(true);
		PEL compte2 = new ServicePEL().creerPEL(sc);
		compte.setNumero(compte2.getNumero());
		assertEquals(compte, compte2);
		sc.close();
	}

	@Test
	public void testCalculerFraisComptePEL() {
		PEL compte = new PEL(true);
		assertEquals(25.0, new ServiceCompte().CalculerFrais(compte));
		compte.setSolde(100.0);
		assertEquals(27.5, new ServiceCompte().CalculerFrais(compte));
	}
}
