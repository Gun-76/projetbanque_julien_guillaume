package fr.afpa.test;

import junit.extensions.ActiveTestSuite;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class TestSuiteProjetBanque extends TestCase {
	public TestSuiteProjetBanque(String name) {
		super(name);
	}
	
	public static TestSuite suite() {
		TestSuite suite = new ActiveTestSuite();
		suite.addTest(new TestSuite(TestAdministrateur.class));
		suite.addTest(new TestSuite(TestAgence.class));
		suite.addTest(new TestSuite(TestClient.class));
		suite.addTest(new TestSuite(TestCompte.class));
		suite.addTest(new TestSuite(TestConseille.class));
		suite.addTest(new TestSuite(TestControle.class));
		suite.addTest(new TestSuite(TestGeneral.class));
		suite.addTest(new TestSuite(TestLivretA.class));
		suite.addTest(new TestSuite(TestPEL.class));
		return suite;
	}
	
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

}
